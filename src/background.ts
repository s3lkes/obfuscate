import browser from "webextension-polyfill";

browser.browserAction?.onClicked.addListener(() => {
  toggle();
});
// Preparing for Manifest V3:
browser.action?.onClicked.addListener(() => {
  toggle();
});

browser.commands.onCommand.addListener(function (command) {
  if (command === "obfuscate" || command === "_execute_page_action") {
    // The "special shortcut" `_execute_page_action` should be sufficient
    // (see https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/commands#special_shortcuts),
    // but since there's no more distinction between page and browser actions in
    // Manifest V3, I've added an explicit command to remain forward compatible:
    toggle();
  }
});

browser.runtime.onMessage.addListener(async (message, _sender) => {
  if (message === "reveal") {
    reveal();
  } else if (message === "obfuscate") {
    obfuscate();
  } else if (message === "obfuscateSelection") {
    obfuscateSelection();
  }
});

async function toggle() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to toggle without a tab being active.");
    return;
  }

  // This script will send us a message telling us to obfuscate or reveal,
  // depending on the current font family. See `browser.runtime.onMessage` above.
  executeScript({
    files: ["/dist/content.js"],
    target: { tabId: activeTabs[0].id },
  })
}

async function obfuscate() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to obfuscate without a tab being active.");
    return;
  }
  injectCss({
    files: ["/src/obfuscate.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}

async function obfuscateSelection() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to obfuscate without a tab being active.");
    return;
  }
  injectCss({
    files: ["/src/obfuscate-selection.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}

async function reveal() {
  const activeTabs = await browser.tabs.query({ active: true, currentWindow: true });
  if (typeof activeTabs[0]?.id !== "number") {
    console.log("Trying to de-obfuscate without a tab being active.");
    return;
  }
  removeCss({
    files: ["/src/obfuscate.css"],
    target: {
      allFrames: true,
      tabId: activeTabs[0]?.id,
    },
  });
}

/**
 * Uses browser.tabs.insertCSS (from Manifest V2) when available,
 * other uses browser.scripting.insertCSS (from Manifest V3).
 */
const injectCss: typeof browser.scripting.insertCSS = (injection) => {
  if (typeof browser.tabs?.insertCSS === "function") {
    return browser.tabs.insertCSS(injection.target.tabId, {
      allFrames: injection.target.allFrames,
      cssOrigin: injection.origin === "USER" ? "user" : "author",
      file: injection.files?.[0],
      code: injection.css,
      frameId: injection.target.frameIds?.[0],
    });
  }
  return browser.scripting.insertCSS(injection);
};

/**
 * Uses browser.tabs.removeCSS (from Manifest V2) when available,
 * other uses browser.scripting.removeCSS (from Manifest V3).
 */
const removeCss: typeof browser.scripting.removeCSS = (injection) => {
  if (typeof browser.tabs?.removeCSS === "function") {
    return browser.tabs.removeCSS(injection.target.tabId, {
      allFrames: injection.target.allFrames,
      cssOrigin: injection.origin === "USER" ? "user" : "author",
      file: injection.files?.[0],
      code: injection.css,
      frameId: injection.target.frameIds?.[0],
    });
  }
  return browser.scripting.removeCSS(injection);
};

const executeScript: typeof browser.scripting.executeScript = (injection) => {
  if (typeof browser.tabs?.executeScript === "function") {
    return browser.tabs.executeScript(injection.target.tabId, {
      allFrames: injection.target.allFrames,
      file: injection.files?.[0],
      code: injection.func?.toString(),
      frameId: injection.target.frameIds?.[0],
    });
  }
  return browser.scripting.executeScript(injection);
};
