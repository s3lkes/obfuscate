## [Unreleased]

## [1.3.0] - 2022-06-05

### New features

- The toggle button is now a so-called "browser action" rather than a "page
  action". This means that it's located in a different place in the UI, which
  has the primary advantage of allowing you to remove/re-order the button. The
  disadvantage is that it will also be visible when it does nothing, e.g. on
  a new tab.

## [1.2.1] - 2022-06-02

### Bugs fixed

- Obfuscating text fields by selecting text in them now also works.

## [1.2.0] - 2022-06-01

### New features

- You can now select text to obfuscate parts of pages. The algorithm is somewhat
  course, so large selections may not obfuscate precisely the parts you're
  interested in, but it will probably satisfy most use cases. If you do need
  more sophisticated selections, please report an issue, then I might invest the
  effort.

### Bugs fixed

- It would not work on some websites (specifically, ones that use `!important`
  in their stylesheets). Obfuscate should now be successful on most of them.

## [1.1.0] - 2022-05-31

### New features

- You can now hit the button/shortcut again to de-obfuscate a page.

## [1.0.0] - 2022-05-30

### New features

- First release! Obfuscate a page using the toolbar button, or by pressing
  `Alt+Shift+O`.
