import commonjs from "@rollup/plugin-commonjs";
import resolve from "@rollup/plugin-node-resolve";
import typescript from "@rollup/plugin-typescript";

export default [
  {
    input: "src/background.ts",
    output: {
      dir: "dist",
      format: "iife"
    },
    plugins: [commonjs(), resolve(), typescript()]
  },
  {
    input: "src/content.ts",
    output: {
      dir: "dist",
      format: "iife"
    },
    plugins: [commonjs(), resolve(), typescript()]
  }
];
