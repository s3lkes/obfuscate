if (!process.env.CI_JOB_ID) {
  console.error("No CI environment variables detected.");
  process.exit(1);
}

const fs = require("fs");
const semver = require("semver");
const manifestJson = require("./manifest.json");
const packageJson = require("./package.json");

if (process.env.CI_COMMIT_TAG) {
  // If we're releasing a stable version, make sure the version is equal in
  // the tag, package.json, and manifest.json
  if (process.env.CI_COMMIT_TAG !== `v${packageJson.version}`) {
    console.error('Version is package.json is not equal to the tag name');
    process.exit(1);
  }
  manifestJson.version = packageJson.version;
} else {
  // If we're releasing an unstable version, make sure its version number
  // does not match a published stable version
  const prereleaseTag = `build${process.env.CI_JOB_ID}`;
  // Make sure the version is at least higher than the currently published version.
  // This will not be problematic when we actually release that version, since we append `prereleaseTag`.
  const prereleaseVersion = semver.inc(manifestJson.version, 'patch') + prereleaseTag;
  manifestJson.version = prereleaseVersion;
  packageJson.version = prereleaseVersion;
}

fs.writeFileSync("./manifest.json", JSON.stringify(manifestJson));
fs.writeFileSync("./package.json", JSON.stringify(packageJson));
